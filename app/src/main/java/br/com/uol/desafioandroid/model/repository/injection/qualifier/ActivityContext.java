package br.com.uol.desafioandroid.model.repository.injection.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ActivityContext {
}
