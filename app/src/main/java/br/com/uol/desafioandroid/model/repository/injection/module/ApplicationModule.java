package br.com.uol.desafioandroid.model.repository.injection.module;

import android.content.Context;

import br.com.uol.desafioandroid.DesafioAndroid;
import br.com.uol.desafioandroid.model.repository.injection.manager.AppDataManager;
import br.com.uol.desafioandroid.model.repository.injection.manager.DataManager;
import br.com.uol.desafioandroid.model.repository.injection.qualifier.ApplicationContext;
import br.com.uol.desafioandroid.model.repository.injection.scope.ApplicationScope;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private DesafioAndroid app;

    public ApplicationModule(DesafioAndroid app) {
        this.app = app;
    }

    @ApplicationContext
    @Provides
    Context provideContext() {
        return app;
    }

    @ApplicationScope
    @Provides
    DataManager provideDataManger(AppDataManager appDataManager) {
        return appDataManager;
    }

}
