package br.com.uol.desafioandroid.model.repository.injection.component;

import br.com.uol.desafioandroid.DesafioAndroid;
import br.com.uol.desafioandroid.model.repository.injection.manager.DataManager;
import br.com.uol.desafioandroid.model.repository.injection.module.ApplicationModule;
import br.com.uol.desafioandroid.model.repository.injection.module.NetworkModule;
import br.com.uol.desafioandroid.model.repository.injection.scope.ApplicationScope;
import dagger.Component;

@ApplicationScope
@Component(modules = {ApplicationModule.class , NetworkModule.class})
public interface ApplicationComponent {

    void inject(DesafioAndroid app);

    DataManager getDataManager();

}
