package br.com.uol.desafioandroid.view;

public interface IBaseView {

    void showError(int error_message);

    void showLoading(boolean bottomProgress);

    void hideLoading(boolean bottomProgress);

    boolean isNetworkAvailable();
}
