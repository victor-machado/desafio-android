package br.com.uol.desafioandroid.model.repository.injection.manager;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import br.com.uol.desafioandroid.model.entity.dto.ShotDTO;
import br.com.uol.desafioandroid.model.repository.injection.qualifier.ApplicationContext;
import br.com.uol.desafioandroid.model.repository.service.INetworkService;
import retrofit2.Call;

public class AppDataManager implements DataManager{

    private INetworkService mService;

    @Inject
    public AppDataManager(@ApplicationContext Context context, INetworkService service) {
        this.mService = service;
    }

    @Override
    public Call<List<ShotDTO>> getShotList(String apiToken, String sort, String pageNumber) {
        return mService.getShotList(apiToken, sort, pageNumber);
    }
}
