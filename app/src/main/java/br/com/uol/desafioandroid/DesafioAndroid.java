package br.com.uol.desafioandroid;

import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

import br.com.uol.desafioandroid.model.repository.injection.component.ApplicationComponent;
import br.com.uol.desafioandroid.model.repository.injection.component.DaggerApplicationComponent;
import br.com.uol.desafioandroid.model.repository.injection.module.ApplicationModule;
import br.com.uol.desafioandroid.model.repository.injection.module.NetworkModule;
import br.com.uol.desafioandroid.model.repository.injection.qualifier.ApplicationContext;

public class DesafioAndroid extends Application {

    private ApplicationComponent mApplicationComponent;

    @Inject
    @ApplicationContext
    Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .networkModule(new NetworkModule())
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);

    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    public Context getApplicationContext() {
        return applicationContext;
    }
}
