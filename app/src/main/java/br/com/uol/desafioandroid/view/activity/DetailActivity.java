package br.com.uol.desafioandroid.view.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.model.entity.vo.ShotVO;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity implements IDetailView{

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.shot_title)
    TextView shotTitle;

    @BindView(R.id.shot_author)
    TextView shotAuthor;

    @BindView(R.id.shot_image)
    ImageView shotImage;

    @BindView(R.id.description)
    TextView description;

    private ShotVO detailShot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        detailShot = (ShotVO) getIntent().getSerializableExtra("shot");

        getActivityComponent().injectDetailActivity(this);
        ButterKnife.bind(this);

        setUpActivity();
    }

    @Override
    public void setUpActivity() {
        getSupportActionBar().setTitle(getResources().getString(R.string.detail_title));

        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);

        Picasso.with(this).load(detailShot.getImage()).into(shotImage);

        showShotDetails();
    }

    @Override
    public void update(int scroll) {

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoading(boolean bottomProgress) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading(boolean bottomProgress) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void imageClicked(ShotVO shot, ImageView image) {

    }

    @Override
    public void showShotDetails() {
        shotTitle.setText(detailShot.getTitle());
        if(detailShot.getDescription() != null){
            description.setText(Html.fromHtml(detailShot.getDescription()));
        }
        shotAuthor.setText(getResources().getString(R.string.shot_author) + " " + detailShot.getAuthor());
    }
}
