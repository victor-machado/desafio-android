package br.com.uol.desafioandroid.view.activity;

import java.util.List;

import br.com.uol.desafioandroid.model.entity.vo.ShotVO;
import br.com.uol.desafioandroid.view.IBaseView;

public interface IHomeView extends IBaseView {

    void responseList(List<ShotVO> result);
}
