package br.com.uol.desafioandroid.presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.model.entity.dto.ShotDTO;
import br.com.uol.desafioandroid.model.entity.vo.ShotVO;
import br.com.uol.desafioandroid.model.repository.Constants;
import br.com.uol.desafioandroid.model.repository.injection.manager.DataManager;
import br.com.uol.desafioandroid.view.activity.IHomeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter<V extends IHomeView> extends BasePresenter<V> implements IHomePresenter<V> {

    private boolean bottomProgress = false;

    @Inject
    HomePresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void getShots(String sort, String pageNumber) {

        if (!getmView().isNetworkAvailable()){
            getmView().showError(R.string.connection_error);
            return;
        }

        if (!pageNumber.equals("1")) {
            bottomProgress = true;
        }

        getmView().showLoading(bottomProgress);

        getDataManager().getShotList("Bearer " + Constants.API_TOKEN, sort, pageNumber).enqueue(new Callback<List<ShotDTO>>() {
            @Override
            public void onResponse(Call<List<ShotDTO>> call, Response<List<ShotDTO>> response) {
                getmView().responseList(parseShotList(response.body()));
                getmView().hideLoading(bottomProgress);
            }

            @Override
            public void onFailure(Call<List<ShotDTO>> call, Throwable t) {
                getmView().showError(R.string.server_error);
                getmView().hideLoading(bottomProgress);
            }
        });
    }

    private List<ShotVO> parseShotList(List<ShotDTO> list){
        List<ShotVO> parsedList = new ArrayList<>();
        for (ShotDTO dto : list) {
            parsedList.add(dto.parseShotVO());
        }
        return parsedList;
    }
}
