package br.com.uol.desafioandroid.presenter;

import javax.inject.Inject;

import br.com.uol.desafioandroid.model.repository.injection.manager.DataManager;
import br.com.uol.desafioandroid.view.IBaseView;

public class BasePresenter<V extends IBaseView> implements IBasePresenter<V>{

    private V mView;
    private DataManager dataManager;

    @Inject
    public BasePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void onAttach(V view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    public V getmView() {
        return mView;
    }

    public DataManager getDataManager() {
        return dataManager;
    }
}
