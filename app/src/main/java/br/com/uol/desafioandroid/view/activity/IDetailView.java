package br.com.uol.desafioandroid.view.activity;

import br.com.uol.desafioandroid.view.IBaseView;

public interface IDetailView extends IBaseView{

    void showShotDetails();
}
