package br.com.uol.desafioandroid.model.repository.injection.module;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import br.com.uol.desafioandroid.model.entity.vo.ShotVO;
import br.com.uol.desafioandroid.model.repository.injection.qualifier.ActivityContext;
import br.com.uol.desafioandroid.model.repository.injection.scope.ActivityScope;
import br.com.uol.desafioandroid.model.repository.service.INetworkService;
import br.com.uol.desafioandroid.presenter.HomePresenter;
import br.com.uol.desafioandroid.presenter.IHomePresenter;
import br.com.uol.desafioandroid.view.activity.IHomeView;
import br.com.uol.desafioandroid.view.adapter.ShotsListAdapter;
import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @ActivityContext
    @ActivityScope
    @Provides
    Context provideContext() {
        return activity;
    }

    @ActivityScope
    @Provides
    Activity provideActivity() {
        return activity;
    }

    @ActivityScope
    @Provides
    INetworkService provideShotService(INetworkService shotService) {
        return shotService;
    }

    @ActivityScope
    @Provides
    IHomePresenter<IHomeView> provideHomePresenter(HomePresenter<IHomeView> homePresenter) {
        return homePresenter;
    }

    @Provides
    ShotsListAdapter provideShotsListAdapter() {
        return new ShotsListAdapter(new ArrayList<ShotVO>());
    }
}
