package br.com.uol.desafioandroid.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.model.entity.vo.ShotVO;

public class ShotsListAdapter extends BaseAdapter {

    private List<ShotVO> shotsList;
    private ImageClickListener listener;

    @Inject
    public ShotsListAdapter(ArrayList<ShotVO> shotsList) {
        this.shotsList = shotsList;
    }

    public void updateList(ArrayList<ShotVO> newShots){
        shotsList.addAll(newShots);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return shotsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    public void setImageClickListener(ImageClickListener listener){
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ShotsListViewHolder holder;

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView != null) {
            holder = (ShotsListViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.shot_preview, parent, false);
            holder = new ShotsListViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.setUp(parent.getContext(), shotsList.get(position));
        holder.shotImagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listener.imageClicked(shotsList.get(position), holder.shotImagePreview);
            }
        });

        return convertView;
    }

    public interface ImageClickListener {

        void imageClicked(ShotVO shot, ImageView image);
    }
}
