package br.com.uol.desafioandroid.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.model.entity.vo.ShotVO;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShotsListViewHolder {

    @BindView(R.id.shot)
    ImageView shotImagePreview;

    public ShotsListViewHolder(View view){
        ButterKnife.bind(this, view);
    }

    public void setUp(Context context, ShotVO shot){

        Picasso.with(context).load(shot.getPreviewImage()).into(shotImagePreview);
    }
}
