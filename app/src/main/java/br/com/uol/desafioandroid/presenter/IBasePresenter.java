package br.com.uol.desafioandroid.presenter;

public interface IBasePresenter<V> {

    void onAttach(V view);

    void onDetach();

}