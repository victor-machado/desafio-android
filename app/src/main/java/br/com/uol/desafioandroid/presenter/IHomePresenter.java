package br.com.uol.desafioandroid.presenter;

import br.com.uol.desafioandroid.view.activity.IHomeView;

public interface IHomePresenter<V extends IHomeView> extends IBasePresenter<V> {

    void getShots(String sort, String pageNumber);
}
