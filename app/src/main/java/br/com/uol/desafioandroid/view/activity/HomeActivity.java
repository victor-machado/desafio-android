package br.com.uol.desafioandroid.view.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.ArraySet;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.model.entity.vo.ShotVO;
import br.com.uol.desafioandroid.presenter.IHomePresenter;
import br.com.uol.desafioandroid.view.adapter.ShotsListAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;

public class HomeActivity extends BaseActivity implements IHomeView {

    @BindView(R.id.sort_spinner)
    Spinner sortSpinner;

    @BindView(R.id.grid_view)
    public GridView gridView;

    @BindView(R.id.fetch_once_progress)
    public ProgressBar progressBar;

    @BindView(R.id.bottom_progress_bar)
    public ProgressBar bottomProgressBar;

    @BindView(R.id.swipe_refresh)
    public SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    IHomePresenter<IHomeView> homePresenter;

    @Inject
    ShotsListAdapter adapter;

    private int scrollCounter = 1;

    private String sort = "popular";

    private boolean avoidSpinner = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getActivityComponent().injectHomeActivity(this);
        ButterKnife.bind(this);

        setUpActivity();
    }

    @Override
    public void setUpActivity() {
        homePresenter.onAttach(this);
        homePresenter.getShots(sort, "1");
        adapter.setImageClickListener(this);
        gridView.setAdapter(adapter);

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView list, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (list.getAdapter() != null
                        && list.getLastVisiblePosition() == list.getAdapter().getCount() - 1
                        && list.getChildAt(list.getChildCount() - 1) != null
                        && list.getChildAt(list.getChildCount() - 1).getBottom() <= list.getHeight()) {

                    if (bottomProgressBar.getVisibility() == View.GONE) {
                        scrollCounter += 1;
                        update(scrollCounter);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView list, int scrollState) {

            }
        });

        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);

        List<String> sortList = new ArrayList<>();
        sortList.add(getResources().getString(R.string.popular));
        sortList.add(getResources().getString(R.string.recent));

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sortList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(spinnerAdapter);

        getSupportActionBar().setTitle(getResources().getString(R.string.list_title));
    }

    @Override
    public void update(int scroll) {
        homePresenter.getShots(sort, "" + scroll);
    }

    @Override
    public void responseList(List<ShotVO> result) {
        adapter.updateList((ArrayList<ShotVO>)result);
    }

    @Override
    public void showLoading(boolean bottomProgress) {
        if (bottomProgress) {
            bottomProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading(boolean bottomProgress) {
        if (bottomProgress) {
            bottomProgressBar.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void imageClicked(ShotVO shot, ImageView image) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("shot", shot);

        if (Build.VERSION.SDK_INT >= 21) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, image, getString(R.string.transition));
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        homePresenter.getShots(sort, "1");
        swipeRefreshLayout.setRefreshing(false);
    }

    @OnItemSelected(R.id.sort_spinner)
    public void spinnerItemSelected(Spinner spinner, int position) {
        if(!avoidSpinner){
            sort = (String) spinner.getAdapter().getItem(position);
            homePresenter.getShots(sort, "1");
        }
        avoidSpinner = false;
    }
}
