package br.com.uol.desafioandroid.model.repository.service;

import java.util.List;

import br.com.uol.desafioandroid.model.entity.dto.ShotDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface INetworkService {

    @GET("shots")
    Call<List<ShotDTO>> getShotList(@Header("Authorization") String apiToken, @Query("sort") String sort, @Query("page") String pageNumber);
}
