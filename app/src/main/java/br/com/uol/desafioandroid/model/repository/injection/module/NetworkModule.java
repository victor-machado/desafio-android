package br.com.uol.desafioandroid.model.repository.injection.module;

import br.com.uol.desafioandroid.model.repository.Constants;
import br.com.uol.desafioandroid.model.repository.injection.qualifier.Url;
import br.com.uol.desafioandroid.model.repository.injection.scope.ApplicationScope;
import br.com.uol.desafioandroid.model.repository.service.INetworkService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @ApplicationScope
    @Provides
    Retrofit provideRetrofit(@Url String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Url
    @Provides
    String retrofitUrl() {
        return Constants.BASE_URL;
    }

    @ApplicationScope
    @Provides
    INetworkService getMovieDbService(Retrofit retrofit) {
        return  retrofit.create(INetworkService.class);
    }
}
