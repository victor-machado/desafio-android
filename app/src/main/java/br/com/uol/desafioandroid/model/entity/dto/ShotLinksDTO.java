package br.com.uol.desafioandroid.model.entity.dto;

import com.google.gson.annotations.SerializedName;

public class ShotLinksDTO {

    @SerializedName("web")
    private String web;

    @SerializedName("twitter")
    private String twitter;

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }
}
