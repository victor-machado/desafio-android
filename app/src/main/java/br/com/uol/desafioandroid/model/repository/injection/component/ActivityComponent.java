package br.com.uol.desafioandroid.model.repository.injection.component;

import br.com.uol.desafioandroid.model.repository.injection.module.ActivityModule;
import br.com.uol.desafioandroid.model.repository.injection.scope.ActivityScope;
import br.com.uol.desafioandroid.view.activity.DetailActivity;
import br.com.uol.desafioandroid.view.activity.HomeActivity;
import dagger.Component;

@ActivityScope
@Component(modules = {ActivityModule.class}, dependencies = {ApplicationComponent.class})
public interface ActivityComponent {

    void injectHomeActivity(HomeActivity homeActivity);

    void injectDetailActivity(DetailActivity detailActivity);
}
