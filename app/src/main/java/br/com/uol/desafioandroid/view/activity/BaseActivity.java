package br.com.uol.desafioandroid.view.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;


import br.com.uol.desafioandroid.DesafioAndroid;
import br.com.uol.desafioandroid.model.repository.injection.component.ActivityComponent;
import br.com.uol.desafioandroid.model.repository.injection.component.DaggerActivityComponent;
import br.com.uol.desafioandroid.model.repository.injection.module.ActivityModule;
import br.com.uol.desafioandroid.view.IBaseView;
import br.com.uol.desafioandroid.view.adapter.ShotsListAdapter;

public abstract class BaseActivity extends AppCompatActivity implements IBaseView, SwipeRefreshLayout.OnRefreshListener, ShotsListAdapter.ImageClickListener {

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityComponent = DaggerActivityComponent.builder()
                .applicationComponent(((DesafioAndroid) getApplication()).getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    @Override
    public void showError(@StringRes int error_message) {
        Snackbar.make(findViewById(android.R.id.content), getString(error_message), Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public abstract void setUpActivity();

    public abstract void update(int scroll);
}
